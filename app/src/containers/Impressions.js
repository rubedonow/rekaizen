import ReactDOM from 'react-dom'
import React from 'react'
import { Bus } from '../bus'
import ImpressionsEnsemble from '../views/ensembles/ImpressionsEnsemble'

class Impressions {
  constructor () {
    this.state = {
      show: false,
      showAddImpressions: false,
      translations: {},
      textareaValue: '',
      issue: '',
      impressions: [],
      id: {
        title: 'impressions-title',
        buttonToggle: 'impressions-button',
        textArea: 'impressions-textarea',
        list: 'impressions-list'
      }
    }
    this.subscribe()

    this.handleToggle = this.handleToggle.bind(this)
    this.closeWithEsc = this.closeWithEsc.bind(this)
    this.addNewImpression = this.addNewImpression.bind(this)
    this.updateNewImpression = this.updateNewImpression.bind(this)
  }

  handleToggle () {
    this.setState({
      showAddImpressions: !this.state.showAddImpressions,
      textareaValue: ''
    })
  }

  closeWithEsc (event) {
    if (event.key === 'Escape') {
      this.setState({
        showAddImpressions: false,
        textareaValue: ''
      })
    }
  }

  setState (value) {
    this.state = Object.assign(this.state, value)
    this.render()
  }

  setImpressions (params) {
    this.setState({ impressions: params.impressions })
  }

  subscribe () {
    Bus.subscribe('got.translations', this.setTranslations.bind(this))
    Bus.subscribe('issue.confirmed', this.isCardListVisible.bind(this))
    Bus.subscribe('impression.stored', this.setImpressions.bind(this))
  }

  isCardListVisible (payload) {
    this.setState({ show: true, issue: payload.issue })
  }

  cleanTextArea () {
    this.setState({ textareaValue: '' })
  }

  addNewImpression () {
    const textareaValue = this.state.textareaValue
    Bus.publish('impression.added', {
      issue: this.state.issue,
      impression: textareaValue
    })
    this.cleanTextArea()
  }

  updateNewImpression (impression) {
    this.setState({ textareaValue: impression })
  }

  render () {
    ReactDOM.render(
      <ImpressionsEnsemble
        show={this.state.show}
        handleToggle={this.handleToggle}
        closeWithEsc={this.closeWithEsc}
        showAddItems={this.state.showAddImpressions}
        title={this.state.translations.title}
        items={this.state.impressions}
        id={this.state.id}
        itemsubmit={this.addNewImpression}
        onChange={this.updateNewImpression}
        placeholder={this.state.translations.placeholder}
        value={this.state.textareaValue}
        buttonTextToggle={this.state.translations.buttonText}
      />,
      document.querySelector('#impression')
    )
  }
  setTranslations (params) {
    this.setState({ translations: params.impression })
  }
}

export default Impressions
