import React from 'react'
import ReactDOM from 'react-dom'
import { Bus } from '../bus'
import IssueEnsemble from '../views/ensembles/IssueEnsemble'
import RetroPage from '../views/RetroPage'

class Issue {
  constructor () {
    this.state = {
      issue: '',
      translations: {},
      showRetroPage: false,
      showConfirmModal: false,
      showProposeModal: false
    }

    this.confirmIssue = this.confirmIssue.bind(this)
    this.cancelIssue = this.cancelIssue.bind(this)
    this.backToHome = this.backToHome.bind(this)
    this.updateNameIssue = this.updateNameIssue.bind(this)
    this.saveProposeIssue = this.saveProposeIssue.bind(this)

    this.subscribe()
  }

  setState (value) {
    this.state = Object.assign(this.state, value)
    this.render()
  }

  subscribe () {
    Bus.subscribe('got.translations', this.setTranslations.bind(this))
    Bus.subscribe('retro.start', this.showView.bind(this))
  }

  setTranslations (params) {
    this.setState({ translations: params.issue })
  }

  showView () {
    this.setState({
      showRetroPage: true,
      showProposeModal: true
    })
  }

  confirmIssue (value) {
    this.setState({
      showConfirmModal: false
    })
    Bus.publish('issue.confirm', { issue: value })
  }

  cancelIssue () {
    this.setState({
      showConfirmModal: false,
      showProposeModal: true
    })
    this.render()
  }

  updateNameIssue (proposedIssue) {
    this.setState({ issue: proposedIssue })
  }

  saveProposeIssue () {
    this.setState({
      showProposeModal: false,
      showConfirmModal: true
    })
  }

  backToHome () {
    this.setState({
      showConfirmModal: false,
      showProposeModal: false,
      showRetroPage: false,
      issue: ''
    })
    Bus.publish('issue.cancelled')
  }

  renderRetroPage () {
    ReactDOM.render(
      <RetroPage title={`El tema a tratar es ${this.state.issue}`} show={this.state.showRetroPage}/>,
      document.querySelector('#retro-page')
    )
  }

  renderIssueEnsemble () {
    ReactDOM.render(
      <IssueEnsemble
        showProposeModal={this.state.showProposeModal}
        value={this.state.issue}
        issueText={this.state.translations}
        onChange={this.updateNameIssue}
        onClick={this.saveProposeIssue}
        backToHome={this.backToHome}
        showConfirmModal={this.state.showConfirmModal}
        issueModalText={this.state.translations}
        issue={ this.state.issue}
        confirmIssue={this.confirmIssue}
        cancelIssue={this.cancelIssue}
      />,
      document.querySelector('#issue-ensemble')

    )
  }

  render () {
    this.renderRetroPage()
    this.renderIssueEnsemble()
  }
}

export default Issue
