import ReactDOM from 'react-dom'
import React from 'react'
import { Bus } from '../bus'
import HomeLogo from '../views/HomeLogo'

class Home {
  constructor () {
    this.state = {
      translations: {},
      showHome: true
    }
    this.handleRetroStart = this.handleRetroStart.bind(this)
    this.subscribe()
  }

  subscribe () {
    Bus.subscribe('got.translations', this.setTranslations.bind(this))
    Bus.subscribe('issue.cancelled', this.toggleView.bind(this))
  }

  setState (value) {
    this.state = Object.assign(this.state, value)
    this.render()
  }

  setTranslations (params) {
    this.setState({ translations: params.home })
  }

  toggleView () {
    this.setState({
      showHome: !this.state.showHome
    })
  }

  handleRetroStart () {
    Bus.publish('retro.start')
    this.toggleView()
  }

  render () {
    ReactDOM.render(
      <HomeLogo
        show={this.state.showHome}
        buttonText={this.state.translations.startButton}
        onRetroStart={this.handleRetroStart.bind(this)}
      />,
      document.querySelector('#home')
    )
  }
}

export default Home
