import React from 'react'
import ReactDOM from 'react-dom'
import { Bus } from '../bus'

import ImprovementActionEnsemble from '../views/ensembles/ImprovementActionEnsemble'

class ImprovementAction {
  constructor () {
    this.state = {
      show: false,
      showAddImprovementAction: false,
      translations: {},
      textareaValue: '',
      issue: '',
      improvementActions: [],
      showSmartGuide: false,
      id: {
        title: 'add-improvement-action',
        buttonToggle: 'button-show-add-improvement-action',
        textarea: 'textarea-improvement',
        button: 'improvement-action-confirm',
        list: 'list-improvement',
        buttonSmart: 'button-show-smart-guide'
      }
    }
    this.subscriptions()

    this.handleToggle = this.handleToggle.bind(this)
    this.closeWithEsc = this.closeWithEsc.bind(this)
    this.addNewImprovementAction = this.addNewImprovementAction.bind(this)
    this.updateNewImprovementAction = this.updateNewImprovementAction.bind(this)
    this.removeImprovementAction = this.removeImprovementAction.bind(this)
    this.handleShowSmartGuide = this.handleShowSmartGuide.bind(this)
  }

  handleToggle () {
    this.setState({
      showAddImprovementAction: !this.state.showAddImprovementAction
    })
  }

  closeWithEsc (event) {
    if (event.key === 'Escape') {
      this.setState({ showAddImprovementAction: false })
    }
  }

  setState (value) {
    this.state = Object.assign(this.state, value)
    this.render()
  }

  setImprovementActions (params) {
    this.setState({ improvementActions: params.improvementActions })
  }

  subscriptions () {
    Bus.subscribe('got.translations', this.setTranslations.bind(this))
    Bus.subscribe('issue.confirmed', this.isListVisible.bind(this))
    Bus.subscribe('improvement.stored', this.setImprovementActions.bind(this))
  }

  isListVisible (payload) {
    this.setState({ show: true, issue: payload.issue })
  }

  cleanTextArea () {
    this.setState({ textareaValue: '' })
  }

  addNewImprovementAction () {
    const textareaValue = this.state.textareaValue
    Bus.publish('improvement.added', {
      issue: this.state.issue,
      improvementAction: textareaValue
    })
    this.cleanTextArea()
  }

  updateNewImprovementAction (improvementAction) {
    this.setState({ textareaValue: improvementAction })
  }

  removeImprovementAction (action) {
    Bus.publish('improvement.delete', {
      issue: this.state.issue,
      improvementAction: action
    })
  }

  setTranslations (params) {
    this.setState({ translations: params.improvementAction })
  }

  handleShowSmartGuide () {
    this.setState({ showSmartGuide: true })
  }

  handleCloseSmartGuide () {
    this.setState({ showSmartGuide: false })
  }

  render () {
    ReactDOM.render(
      <ImprovementActionEnsemble
        show={this.state.show}
        handleToggle={this.handleToggle}
        closeWithEsc={this.closeWithEsc}
        showAddItems={this.state.showAddImprovementAction}
        title={this.state.translations.title}
        items={this.state.improvementActions}
        id={this.state.id}
        itemsubmit={this.addNewImprovementAction}
        onChange={this.updateNewImprovementAction}
        placeholder={this.state.translations.placeholder}
        confirmAction={this.state.translations.confirm}
        value={this.state.textareaValue}
        buttonTextToggle={this.state.translations.buttonText}
        handleClick={this.removeImprovementAction}
        handleShowSmartGuide={this.handleShowSmartGuide}
        showSmartGuide={this.state.showSmartGuide}
        handleCloseSmartGuide={this.handleCloseSmartGuide}
      />,
      document.querySelector('#improvement-action-list')
    )
  }
}

export default ImprovementAction
