import { Bus } from '../bus'

class Impressions {
  constructor () {
    this.subscriptions()
  }

  subscriptions () {
    Bus.subscribe('impression.added', this.saveAndPublish.bind(this))
  }

  saveAndPublish (payload) {
    this.save(payload)
    this.publish(payload.issue)
  }

  save (payload) {
    const issue = payload.issue
    const impressionNoNewlines = this.removeNewLines(payload.impression)
    const storedImpressions = this.retrieveData(issue)
    storedImpressions.push(impressionNoNewlines)
    this.storeData(issue, storedImpressions)
  }

  publish (issue) {
    const storedImpressions = this.retrieveData(issue)
    Bus.publish('impression.stored', { issue: issue, impressions: storedImpressions })
  }

  retrieveData (key) {
    const storedValue = this.getStoredValue(key)
    const impressionsStored = storedValue.impressions || []
    return impressionsStored
  }

  storeData (key, value) {
    const storedValue = this.getStoredValue(key)
    storedValue.impressions = value
    window.localStorage.setItem(key, JSON.stringify(storedValue))
  }

  getStoredValue (key) {
    return JSON.parse(window.localStorage.getItem(key)) || {}
  }

  removeNewLines (text) {
    return text.replace(/[\n]/g, '')
  }
}

export default Impressions
