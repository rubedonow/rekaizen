import { Bus } from '../bus'
import { APIClient } from '../api_client'
export default class Translations {
  constructor () {
    this.client = APIClient
    this.subscriptions()
  }

  subscriptions () {
  }

  retrieveTranslations () {
    let callback = this.buildCallback('got.translations')
    let body = {}
    let url = 'translate'
    this.client.hit(url, body, callback)
  }

  buildCallback (signal) {
    return function (response) {
      Bus.publish(signal, response)
    }
  }
}
