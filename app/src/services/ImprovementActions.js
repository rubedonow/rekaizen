import { Bus } from '../bus'

class ImprovementActions {
  constructor () {
    this.subscriptions()
  }

  subscriptions () {
    Bus.subscribe('improvement.added', this.saveAndPublish.bind(this))
    Bus.subscribe('issue.confirmed', this.publish.bind(this))
    Bus.subscribe('improvement.delete', this.deleteAndPublish.bind(this))
  }

  saveAndPublish (payload) {
    this.save(payload)
    this.publish(payload.issue)
  }

  deleteAndPublish (payload) {
    this.delete(payload)
    this.publish(payload.issue)
  }

  save (payload) {
    const issue = payload.issue
    const improvementActionNoNewlines = this.removeNewLines(payload.improvementAction)
    const storedImprovementAction = this.retrieveData(issue)
    storedImprovementAction.push(improvementActionNoNewlines)
    this.storeData(issue, storedImprovementAction)
  }

  delete (payload) {
    const issue = payload.issue
    const storedImprovementAction = this.retrieveData(issue)

    var index = storedImprovementAction.indexOf(payload.improvementAction)
    if (index > -1) {
      storedImprovementAction.splice(index, 1)
    }
    this.storeData(issue, storedImprovementAction)
  }

  publish (issue) {
    const improvementActionsStored = this.retrieveData(issue)
    Bus.publish('improvement.stored', { issue: issue, improvementActions: improvementActionsStored })
  }

  retrieveData (key) {
    const storedValue = this.getStoredValue(key)
    const improvementActionsStored = storedValue.improvementActions || []
    return improvementActionsStored
  }

  storeData (key, value) {
    const storedValue = this.getStoredValue(key)
    storedValue.improvementActions = value
    window.localStorage.setItem(key, JSON.stringify(storedValue))
  }

  getStoredValue (key) {
    return JSON.parse(window.localStorage.getItem(key)) || {}
  }

  removeNewLines (text) {
    return text.replace(/[\n]/g, '')
  }
}

export default ImprovementActions
