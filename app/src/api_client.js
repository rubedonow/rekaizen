import aja from 'aja'

export let APIClient = {
  HOST: process.env.API_HOST,
  PORT: process.env.API_PORT,

  hit: function (endpoint, data, action) {
    let baseUrl = `http://${this.HOST}:${this.PORT}/`
    aja()
      .method('post')
      .body(data)
      .url(baseUrl + endpoint)
      .on('success', action)
      .go()
  }
}
