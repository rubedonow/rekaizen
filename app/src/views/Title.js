import React from 'react'
import { Bus } from '../bus'

class Title extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      title: ''
    }
  }

  componentDidMount () {
    Bus.subscribe('got.translations', this.setTitle.bind(this))
  }

  setTitle (params) {
    this.setState({ title: params.home.rekaizen })
  }

  render () {
    return (<a className="has-text-grey" href="#" id="rekaizen">{this.state.title}</a>)
  }
}

export default Title
