import React from 'react'

function classNames (defaultClass = '', classesNames = {}, customClasses = '') {
  const classes = Object.keys(classesNames).filter(key => classesNames[key])
  return [defaultClass, ...classes, customClasses].join(' ').trim()
}

const Button = ({ primary, confirm, cancel, loading, info, className, ...props }) => {
  const classes = classNames('button', {
    'is-dark': primary || confirm,
    'is-light': cancel,
    'is-loading': loading,
    'is-white': info
  }, className)
  return (
    <button className={classes} {...props} >
      {props.children}
    </button>
  )
}

Button.defaultProps = {
  primary: false,
  confirm: false,
  cancel: false,
  info: false,
  className: ''
}

export default Button
