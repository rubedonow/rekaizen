import React from 'react'
class List extends React.Component {
  renderItems (items = []) {
    return items.map((item, index) => (
      <tr key={'item' + index + item}>
        <td>
          {item}
        </td>
      </tr>
    ))
  }
  render () {
    return (
      <table id={this.props.id} className="table is-striped is-hoverable is-fullwidth">
        <tbody>
          {this.renderItems(this.props.items)}
        </tbody>
      </table>
    )
  }
}

List.defaultProps = {
  id: '',
  items: []
}

export default List
