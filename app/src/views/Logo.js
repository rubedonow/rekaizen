import React from 'react'

const Logo = () => (
  <div id="wrapper" style={wrapper}>
    <svg id="logo-wrapper" style={logoWrapper} viewBox="0 0 20 20">
      <circle cx="10" cy="10" r="10" id="circle" style={logoCircle}/>

      <g id="svgGroup" stroke="#FBFBFB" strokeWidth="0.25mm" fill="none">

        <path className="path letter" style={Object.assign({}, logoPath, logoLetter)} d="M 5.864 8.818 L 5.864 9.019 L 5.332 9.019 A 2.62 2.62 0 0 1 4.955 8.991 A 2.925 2.925 0 0 1 4.9 8.982 A 2.01 2.01 0 0 1 4.468 8.86 Q 4.258 8.774 4.072 8.63 A 1.438 1.438 0 0 1 3.804 8.356 A 1.682 1.682 0 0 1 3.745 8.271 A 1.584 1.584 0 0 1 3.6 7.992 A 2.071 2.071 0 0 1 3.523 7.761 A 2.182 2.182 0 0 1 3.463 7.445 Q 3.445 7.295 3.443 7.128 A 3.613 3.613 0 0 1 3.442 7.08 L 3.442 5.967 Q 3.442 5.718 3.362 5.481 Q 3.281 5.244 3.123 5.056 A 1.189 1.189 0 0 0 2.748 4.762 A 1.365 1.365 0 0 0 2.729 4.753 A 1.116 1.116 0 0 0 2.392 4.652 A 1.43 1.43 0 0 0 2.192 4.639 L 2.192 4.458 A 1.273 1.273 0 0 0 2.507 4.42 A 1.082 1.082 0 0 0 2.712 4.346 A 1.215 1.215 0 0 0 3.105 4.048 Q 3.267 3.862 3.354 3.625 Q 3.442 3.389 3.442 3.14 L 3.442 1.958 A 3.562 3.562 0 0 1 3.462 1.57 Q 3.498 1.248 3.596 1.008 A 1.757 1.757 0 0 1 3.763 0.694 A 1.326 1.326 0 0 1 4.009 0.417 Q 4.268 0.2 4.609 0.115 Q 4.951 0.029 5.332 0.029 L 5.864 0.029 L 5.864 0.269 A 2.502 2.502 0 0 0 5.636 0.327 Q 5.525 0.362 5.431 0.405 A 1.432 1.432 0 0 0 5.388 0.425 A 0.968 0.968 0 0 0 5.165 0.575 A 0.858 0.858 0 0 0 5.066 0.681 A 0.956 0.956 0 0 0 4.932 0.917 A 1.222 1.222 0 0 0 4.883 1.067 A 1.683 1.683 0 0 0 4.844 1.275 Q 4.83 1.382 4.826 1.503 A 3.27 3.27 0 0 0 4.824 1.616 L 4.824 2.627 A 3.173 3.173 0 0 1 4.739 3.369 A 1.909 1.909 0 0 1 4.592 3.778 A 1.636 1.636 0 0 1 4.463 3.994 A 1.305 1.305 0 0 1 3.965 4.414 A 1.258 1.258 0 0 1 3.635 4.521 Q 3.485 4.548 3.313 4.548 A 2.272 2.272 0 0 1 3.213 4.546 Q 3.677 4.556 3.987 4.7 A 1.305 1.305 0 0 1 4.333 4.93 A 1.184 1.184 0 0 1 4.482 5.1 A 1.54 1.54 0 0 1 4.694 5.517 A 1.897 1.897 0 0 1 4.746 5.706 A 3.208 3.208 0 0 1 4.812 6.156 A 4.021 4.021 0 0 1 4.824 6.47 L 4.824 7.427 A 5.2 5.2 0 0 0 4.83 7.677 Q 4.836 7.795 4.847 7.896 A 2.325 2.325 0 0 0 4.868 8.044 Q 4.912 8.291 5.029 8.447 A 0.73 0.73 0 0 0 5.263 8.648 A 0.904 0.904 0 0 0 5.347 8.689 Q 5.492 8.751 5.699 8.791 A 3.109 3.109 0 0 0 5.864 8.818 Z M 0 9.009 L 0 0 L 1.323 0 L 1.323 9.009 L 0 9.009 Z"
          vectorEffect="non-scaling-stroke"/>
      </g>
    </svg>
  </div>
)

const logoWrapper = {
  borderRadius: '100%'
}

const wrapper = {
  maxWidth: '35%',
  margin: 'auto'
}

const logoCircle = {
  fill: 'rgba(150, 48, 90, 1)',
  stroke: '#000',
  strokeWidth: '20',
  strokeDasharray: 'calc(3.14159 * 20)',
  strokeDashoffset: 'calc(3.14159 * 20)',
  animation: 'logo__letter__amination--rotate 5s linear infinite',
  transform: 'rotate(-90deg)',
  transformOrigin: '50% 50%'
}

const logoLetter = {
  strokeDasharray: '1100',
  strokeDashoffset: '1100',
  WebkitAnimation: 'logo__letter__amination--dashoffset 0.7s linear 2s forwards, logo__letter__amination--opacity 1s linear 3s forwards',
  animation: 'logo__letter__amination--dashoffset 0.7s linear 2s forwards, logo__letter__amination--opacity 1s linear 3s forwards'
}

const logoPath = {
  stroke: '#FBFBFB',
  fill: '#FBFBFB',
  fillOpacity: '0',
  transform: 'translate(7.8px, 5.4px)'
}

export default Logo
