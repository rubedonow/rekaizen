/* eslint-disable react/jsx-no-duplicate-props */
import React from 'react'
import Modal from './Modal'
import Button from './Button'
import TextAreaAutoSize from './TextAreaAutoSize'

const ProposeIssue = (props) => {
  if (props.showProposeModal) {
    return (
      <Modal onModalClose={props.backToHome}>
        <div className="field">
          <label htmlFor="description-propose-issue" className="label">
            {props.issueText.description}
          </label>
          <div className="control">
            <TextAreaAutoSize
              id="description-propose-issue"
              className='textarea'
              value={props.value}
              placeholder={props.issueText.placeholder}
              onChange={event => props.onChange(event.target.value)}
            />
          </div>
        </div>
        <div className="field is-grouped is-grouped-right">
          <div className="control">
            <Button
              confirm
              id="button-propose-issue"
              onClick={props.onClick}
            >
              {props.issueText.propose}
            </Button>
          </div>
        </div>
      </Modal>
    )
  }
  return null
}

Modal.defaultProps = {
  show: false,
  backToHome: () => {},
  issueText: { description: '', placeholder: '', propose: '' },
  value: '',
  onChange: () => {},
  onClick: () => {}
}

export default ProposeIssue
