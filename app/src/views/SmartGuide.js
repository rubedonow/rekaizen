import React from 'react'
class SmartGuide extends React.Component {
  render () {
    return (
      <article className="message">
        <div className="message-header">
          <p>{this.props.smartGuide.title}</p>
          <button className="delete" aria-label="delete"></button>
        </div>
        <div className="message-body">
          <p>{this.props.smartGuide.definition}</p>
          <article class="message is-dark">
            <div class="message-body">
              <p className="title is-size-3">{this.props.smartGuide.specificTitle}</p>
              <p>{this.props.smartGuide.specific}</p>
            </div>
          </article>
          <article class="message is-dark">
            <div class="message-body">
              <p className="title is-size-3">{this.props.smartGuide.measurableTitle}</p>
              <p>{this.props.smartGuide.measurable}</p>
            </div>
          </article>
          <article class="message is-dark">
            <div class="message-body">
              <p className="title is-size-3">{this.props.smartGuide.attainableTitle}</p>
              <p>{this.props.smartGuide.attainable}</p>
            </div>
          </article>
          <article class="message is-dark">
            <div class="message-body">
              <p className="title is-size-3">{this.props.smartGuide.relevantTitle}</p>
              <p>{this.props.smartGuide.relevant}</p>
            </div>
          </article>
          <article class="message is-dark">
            <div class="message-body">
              <p className="title is-size-3">{this.props.smartGuide.timeboxedTitle}</p>
              <p>{this.props.smartGuide.timeboxed}</p>
            </div>
          </article>
          <article class="message is-dark">
            <div class="message-body">
              <a href= {this.props.smartGuide.video} target="_blank">video</a>
            </div>
          </article>
        </div>
      </article>
    )
  }
}

export default SmartGuide
