import React from 'react'

const RetroPage = (props) => {
  if (props.show) {
    return (
      <div className='section'>{props.title}</div>
    )
  }
  return null
}

RetroPage.defaultProps = {
  show: false,
  title: ''
}
export default RetroPage
