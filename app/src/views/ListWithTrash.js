import React from 'react'
import Button from './Button'

class ListWithTrash extends React.Component {
  renderItems (items = []) {
    return items.map((item, index) => (
      <tr key={'item' + index + item}>
        <td>
          {item}
        </td>
        <td className="has-text-right">
          <Button onClick={this.props.handleClick.bind(this, item)}>
            <span className="icon">
              <i className="fas fa-trash"></i>
            </span>
          </Button>
        </td>
      </tr>
    )
    )
  }
  render () {
    return (
      <table className="table is-striped is-hoverable is-fullwidth">
        <tbody id={this.props.id}>
          {this.renderItems(this.props.items)}
        </tbody>
      </table>
    )
  }
}

ListWithTrash.defaultProps = {
  items: [],
  id: '',
  handleClick: () => {}
}

export default ListWithTrash
