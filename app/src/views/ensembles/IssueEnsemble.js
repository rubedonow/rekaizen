import React from 'react'
import IssueModalContent from '../IssueModalContent'
import ProposeIssue from '../ProposeIssue'

const IssueEnsemble = props => {
  const confirmModalProps = {
    showConfirmModal: props.showConfirmModal,
    issueModalText: props.issueModalText,
    issue: props.issue,
    confirmIssue: props.confirmIssue,
    cancelIssue: props.cancelIssue,
    backToHome: props.backToHome
  }
  const proposeIssueProps = {
    showProposeModal: props.showProposeModal,
    backToHome: props.backToHome,
    issueText: props.issueText,
    value: props.value,
    onChange: props.onChange,
    onClick: props.onClick
  }
  return (
    <div>
      <ProposeIssue {...proposeIssueProps}/>
      <IssueModalContent {...confirmModalProps}/>
    </div>
  )
}

export default IssueEnsemble
