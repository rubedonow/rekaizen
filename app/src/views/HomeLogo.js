import React from 'react'
import Button from './Button'
import Logo from './Logo'

const HomeLogo = (props) => {
  if (props.show) {
    return (
      <div className='has-text-centered section' id='home'>
        <Logo />
        <Button
          primary
          id='button-start-retro'
          onClick={props.onRetroStart}
        >
          {props.buttonText}
        </Button>
      </div>
    )
  }
  return null
}

export default HomeLogo
