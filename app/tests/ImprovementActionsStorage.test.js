import ImprovementActions from '../src/services/ImprovementActions'

describe('The improvement action service', () => {
  test('stores the data to localStorage', () => {
    const payload = {
      issue: 'Test Issue',
      improvementAction: 'Test ImprovementAction'
    }
    const valueToStore = {
      improvementActions: ['Test ImprovementAction']
    }
    const storedValue = JSON.stringify(valueToStore)
    jest.spyOn(Storage.prototype, 'setItem')
    localStorage.clear()

    new ImprovementActions().save(payload)

    expect(localStorage.setItem).toHaveBeenLastCalledWith(payload['issue'], storedValue)
    expect(localStorage.getItem(payload['issue'])).toBe(storedValue)
  })

  test('delete data from localStorage', () => {
    const payload = {
      issue: 'Test Issue',
      improvementAction: 'Test ImprovementAction'
    }
    localStorage.clear()
    new ImprovementActions().save(payload)

    new ImprovementActions().delete(payload)

    expect(JSON.parse(localStorage.getItem(payload['issue'])).improvementActions.includes(payload.improvementAction)).toBe(false)
  })

  test('if there are two equal improvement actions, it deletes just one of them', () => {
    const payload = {
      issue: 'Test Issue',
      improvementAction: 'Test ImprovementAction'
    }
    localStorage.clear()
    new ImprovementActions().save(payload)
    new ImprovementActions().save(payload)

    new ImprovementActions().delete(payload)

    expect(JSON.parse(localStorage.getItem(payload['issue'])).improvementActions.length).toBe(1)
  })
})
