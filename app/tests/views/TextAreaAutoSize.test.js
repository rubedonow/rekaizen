import React from 'react'
import { render } from 'react-testing-library'

import TextAreaAutosize from '../../src/views/TextAreaAutoSize'

describe('text area component', () => {
  test('exists', () => {
    const placeholderText = 'sample text'
    const { getByPlaceholderText } = render(<TextAreaAutosize placeholder={placeholderText}/>)
    const textarea = getByPlaceholderText(placeholderText)
    expect(textarea).not.toBe(undefined)
  })

  test('a value is shown', () => {
    const initialText = 'initial text'

    const { container } = render(<TextAreaAutosize value={initialText}/>)
    const textarea = container.querySelector('textarea')

    expect(textarea.value).toBe(initialText)
  })
})
