import Impressions from '../src/services/Impressions'
import { Bus } from '../src/bus'

describe('The service Impressions', () => {
  test('stores the data to localStorage', () => {
    const payload = {
      issue: 'Test Issue',
      impression: 'Test Impression'
    }
    const valueToStore = {
      impressions: ['Test Impression']
    }
    const storedValue = JSON.stringify(valueToStore)
    jest.spyOn(Storage.prototype, 'setItem')
    jest.spyOn(Storage.prototype, 'getItem')
    localStorage.clear()

    new Impressions().save(payload)

    expect(localStorage.setItem).toHaveBeenLastCalledWith(payload['issue'], storedValue)
    expect(localStorage.getItem(payload['issue'])).toBe(storedValue)
  })

  test('calls the method save when it receives the corresponding message from the Bus', () => {
    const impressionsServiceSave = jest.spyOn(Impressions.prototype, 'save')
    const payload = {
      issue: 'Test Issue',
      impression: 'Test Impression'
    }

    Bus.publish('impression.added', payload)

    expect(impressionsServiceSave).toHaveBeenCalledTimes(1)
    expect(impressionsServiceSave).toHaveBeenLastCalledWith(payload)
  })

  test('publishes a message to the Bus after saving', () => {
    const busPublish = jest.spyOn(Bus, 'publish')
    const payload = {
      issue: 'Test Issue',
      impression: 'Test Impression'
    }

    Bus.publish('impression.added', payload)

    const storedData = new Impressions().retrieveData(payload.issue)
    const message = {
      issue: payload.issue,
      impressions: storedData
    }
    expect(busPublish).toHaveBeenLastCalledWith('impression.stored', message)
  })
})
