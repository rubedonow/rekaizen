import Issues from '../src/services/Issues'

describe('The service Issues', () => {
  test('stores the data to localStorage', () => {
    const payload = {
      issue: 'Test Issue'
    }
    const valueToStore = { }
    const storedValue = JSON.stringify(valueToStore)
    jest.spyOn(Storage.prototype, 'setItem')
    jest.spyOn(Storage.prototype, 'getItem')
    localStorage.clear()

    new Issues().save(payload)

    expect(localStorage.setItem).toHaveBeenLastCalledWith(payload['issue'], storedValue)
    expect(localStorage.getItem(payload['issue'])).toBe(storedValue)
  })
})
