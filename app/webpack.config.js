const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: './src/main.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: path.resolve(__dirname, 'public/dist'),
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      'API_HOST': 'localhost',
      'API_PORT': '3001'
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    hot: true,
    port: 3000,
    host: '0.0.0.0',
    allowedHosts: [
      'app'
    ]
  }
}
