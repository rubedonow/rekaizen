#Rekaizen

## Development dependencies

- `Docker version 18.06.1-ce` or higher.
- `docker-compose version 1.18.0` or higher.

## Running the application

You only need to run the following command in the project folder:

`docker-compose up --build` first time.

`docker-compose up`

> Remember to shutdown the docker container with `docker-compose down` when you finish your work.

## Running for development.

After run the application like is explained before you have to run in a new terminal:

`docker-compose exec app npm run build-watch`

> Remember to run this command after launching any testing script

## Linting

In each of the services you can lint files using Eslint. You can run the script manually in each service by using:

`docker-compose exec SERVICE_NAME npm run lint`

where SERVICE_NAME could be api, app or e2e

## Running the react-component tests

After run the application you have to execute:

`docker-compose exec app npm run test-unit`

## Running the linting and the react-component tests

After run the application you have to execute:

`docker-compose exec app npm run test-all`

## Running the e2e tests (Cypress)

After run the application you have to execute:

- `docker-compose exec app npm run build-test`
- `docker-compose run --rm e2e npm run test-functional`

## Running the e2e tests (Cypress) and LINTER

After run the application you have to execute:

- `docker-compose exec app npm run build-test`
- `docker-compose run --rm e2e npm run test-all`



## Running the api test, linting and unit test

- `docker-compose exec api npm run test-all`
