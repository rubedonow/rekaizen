/* eslint-disable eol-last */
/* eslint-disable no-unused-expressions */
import Rekaizen from './pages/Rekaizen'

describe('new impressions action', () => {
  it('text area to add impressions action can be toggled', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .impressionsTextareaToggle()
    expect(rekaizen.exist('#impressions-textarea')).to.be.true
    rekaizen
      .impressionsTextareaToggle()
    expect(rekaizen.dontExist('#impressions-textarea')).to.be.true
  })
  it('close by pressing the "Escape" key', () => {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .impressionsTextareaToggle()
      .typeEscapeTextareaImprovement()
    expect(rekaizen.dontExist('#impressions-textarea')).to.be.true
  })
})