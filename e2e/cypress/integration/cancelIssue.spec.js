/* eslint-disable no-unused-expressions */
import Rekaizen from './pages/Rekaizen'

describe('issueModalContent has', function () {
  it('a cancel button that when clicked it navigate back and shows Issue proposal', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('Proponemos una Issue')
      .clickOnPropose()
      .cancelIssue()
    expect(rekaizen.contains('Proponemos una Issue')).to.be.true
  })

  it('a user can cancel an Issue and return to Home', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue()
      .clickOnPropose()
      .backToHome()
      .startRetro()
      .getProposedIssue()
    expect(rekaizen.isEmpty()).to.be.true
  })
})
