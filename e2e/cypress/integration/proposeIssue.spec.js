/* eslint-disable no-unused-expressions */
import Rekaizen from './pages/Rekaizen'

describe('Propose Issue', () => {
  it('write a new one', function () {
    const rekaizen = new Rekaizen()
    const text = 'New Issue'

    rekaizen.startRetro().typeIssue(text).clickOnPropose()
    expect(rekaizen.contains(text)).to.be.true
  })
})
