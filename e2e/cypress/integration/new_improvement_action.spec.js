/* eslint-disable no-unused-expressions */
import Rekaizen from './pages/Rekaizen'

describe('new improvement action', () => {
  it('text area to add improvement action can be toggled', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .toggleAddImprovementAction()
    expect(rekaizen.exist('#textarea-improvement')).to.be.true
    rekaizen
      .toggleAddImprovementAction()
    expect(rekaizen.dontExist('#textarea-improvement')).to.be.true
  })

  it('keeps text in textarea when toggled', function () {
    const SAMPLE_TEXT = 'Hola mundo'
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .toggleAddImprovementAction()
      .typeTextareaImprovement(SAMPLE_TEXT)
      .toggleAddImprovementAction()
      .toggleAddImprovementAction()
    expect(rekaizen.textAreaContains(SAMPLE_TEXT)).to.be.true
  })

  it('the help botton show the SMART guide', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .toggleAddImprovementAction()
      .showSmartGuide()

    expect(rekaizen.exist('#smart-guide')).to.be.true
  })
})
