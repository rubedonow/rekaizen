class Rekaizen {
  constructor () {
    cy.visit('/')
  }

  startRetro () {
    cy
      .get('#button-start-retro')
      .click()
    return this
  }

  buttonStartRetro () {
    cy
      .get('#button-start-retro')
    return this
  }

  cancelIssue () {
    cy
      .get('#button-cancel-issue')
      .click()
    return this
  }

  toggleAddImprovementAction () {
    cy
      .get('#button-show-add-improvement-action')
      .click()
    return this
  }

  addImprovementAction () {
    cy
      .get('#add-improvement-action')
    return this
  }

  getProposedIssue () {
    cy
      .get('#description-propose-issue')
    return this
  }

  typeIssue (text) {
    text = text || 'text'
    cy
      .get('#description-propose-issue')
      .type(text)
    return this
  }

  clickOnPropose () {
    cy
      .get('#button-propose-issue')
      .click()
    return this
  }

  confirmIssue () {
    cy
      .get('#button-confirm-issue')
      .click()
    return this
  }

  confirmImprovementAction () {
    cy
      .get('#improvement-action-confirm')
      .click()
    return this
  }

  textAreaContains (text) {
    cy
      .get('#textarea-improvement')
      .contains(text)
    return true
  }

  getTextAreaImprovement () {
    cy
      .get('#textarea-improvement')
    return this
  }

  getListImprovement () {
    cy
      .get('#list-improvement')
    return this
  }

  getListImprovementElements () {
    cy
      .get('#list-improvement')
      .children()
    return this
  }

  typeTextareaImprovement (text) {
    cy
      .get('#textarea-improvement')
      .type(text)
    return this
  }

  typeEnterTextareaImprovement (text) {
    cy
      .get('#textarea-improvement')
      .type(text)
      .type('{enter}')

    return this
  }

  typeEnterTextareaImpression (text) {
    cy
      .get('#impressions-textarea')
      .type(text)
      .type('{enter}')
    return this
  }

  listImprovement () {
    cy
      .get('#list-improvement')
      .children()
    return this
  }

  listImpressions () {
    cy
      .get('#impressions-list')
      .children()
    return this
  }

  contains (value) {
    cy
      .contains(value)
    return true
  }

  isEmpty () {
    cy
      .should('be.empty')
    return true
  }

  hasLength (len) {
    cy
      .should('have.length', len)
    return true
  }

  dontExist (selector) {
    cy
      .get(selector)
      .should('not.exist')
    return true
  }

  should (method) {
    cy
      .should(method)
    return true
  }

  exist (selector) {
    cy
      .get(selector)
      .should('exist')
    return true
  }

  titleIsRekaizen () {
    cy
      .get('#title')
      .contains('ReKaizen')
    return true
  }

  api (method, url, body) {
    const request = cy.request(method, url, body)
    return request
  }

  backToHome () {
    cy
      .get('[data-testid="button-close"]')
      .click()
    return this
  }

  clickFirstListButton () {
    cy
      .get('#list-improvement button')
      .first()
      .click({ force: true })
    return this
  }

  impressionsTextareaToggle () {
    cy
      .get('#impressions-button')
      .click()
    return this
  }

  typeEscapeTextareaImprovement () {
    cy
      .get('#impressions-textarea')
      .type('{esc}')
    return this
  }

  showSmartGuide () {
    cy
      .get('#button-show-smart-guide')
      .click()
    return this
  }
}

export default Rekaizen
