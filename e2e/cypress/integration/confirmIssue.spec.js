/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */

import Rekaizen from './pages/Rekaizen'
describe('Issue confirmation page has a confirm button', function () {
  it('with a description', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue()
      .clickOnPropose()
    expect(rekaizen.contains('Confirmar')).to.be.true
  })
  it('that when clicked it dissapears', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue()
      .clickOnPropose()
      .confirmIssue()
    expect(rekaizen.dontExist('#button-confirm-issue')).to.be.true
  })
  it('that when clicked it calls another page', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue()
      .clickOnPropose()
      .confirmIssue()
    expect(rekaizen.exist('#retro-page')).to.be.true
  })
  it('that when clicked issue is registered', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue()
      .clickOnPropose()
      .confirmIssue()
      .api('POST', 'http://api:3001/api/register', { 'issue': 'EL TEMA HARCODEADO' })
      .then((response) => {
        expect(response.body).to.have.property('message').eql({ 'issue': 'EL TEMA HARCODEADO' }) // true
        expect(response.body).to.have.property('timeStamp')
      })
  })
})
