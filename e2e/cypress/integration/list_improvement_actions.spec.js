/* eslint-disable no-unused-expressions */
import Rekaizen from './pages/Rekaizen'
const SAMPLE_IMPROVEMENT_ACTION = 'Hola mundo'

describe('list improvement actions', () => {
  it('adds a new improvement action by clicking the "Add" button', () => {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .toggleAddImprovementAction()
      .typeTextareaImprovement(SAMPLE_IMPROVEMENT_ACTION)
      .confirmImprovementAction()
      .getListImprovement()
    expect(rekaizen.contains(SAMPLE_IMPROVEMENT_ACTION)).to.be.true
  })

  it('adds a new improvement action by pressing the "Enter" key', () => {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .toggleAddImprovementAction()
      .typeEnterTextareaImprovement(SAMPLE_IMPROVEMENT_ACTION)
      .confirmImprovementAction()
      .getListImprovement()
    expect(rekaizen.contains(SAMPLE_IMPROVEMENT_ACTION)).to.be.true
  })

  it('when adding a new improvement action the textarea clears', () => {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .toggleAddImprovementAction()
      .typeEnterTextareaImprovement(SAMPLE_IMPROVEMENT_ACTION)
      .getTextAreaImprovement()
    expect(rekaizen.isEmpty()).to.be.true
  })

  it('improvement action list has several elements', () => {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .toggleAddImprovementAction()
      .typeEnterTextareaImprovement(SAMPLE_IMPROVEMENT_ACTION)
      .typeEnterTextareaImprovement(SAMPLE_IMPROVEMENT_ACTION)
      .typeEnterTextareaImprovement(SAMPLE_IMPROVEMENT_ACTION)
      .typeEnterTextareaImprovement(SAMPLE_IMPROVEMENT_ACTION)
      .getListImprovementElements()
    expect(rekaizen.hasLength(4)).to.be.true
  })

  it('improvement action can be deleted', () => {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .toggleAddImprovementAction()
      .typeEnterTextareaImprovement(SAMPLE_IMPROVEMENT_ACTION)
      .typeEnterTextareaImprovement(SAMPLE_IMPROVEMENT_ACTION)
      .clickFirstListButton()
      .getListImprovementElements()
    expect(rekaizen.hasLength(1)).to.be.true
  })
})
