const TranslateAll = require('./actions/TranslateAll')
const RegisterAll = require('./actions/RegisterAll')
const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const api = express()
const router = express.Router()
const cors = require('cors')
const bodyParser = require('body-parser')

api.use(cors())
api.options('*', cors())
api.use(logger('dev'))
api.use(express.json())
api.use(express.urlencoded({ extended: false }))
api.use(cookieParser())
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))

router.post('/translate', function (request, response) {
  const message = TranslateAll.do()

  response.send(JSON.stringify(message))
})

router.post('/api/register', function (request, response) {
  const message = request.body
  const registry = RegisterAll.createRegistry(message)

  response.send(JSON.stringify(registry))
})

api.use('/', router)

module.exports = api
