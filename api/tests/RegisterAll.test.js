const api = require('../api')
const request = require('supertest')

describe('Api', () => {
  test('creates a registry', async () => {
    const issue = { issue: 'la issue' }
    await request(api)
      .post('/api/register')
      .send(issue)
      .set('Accept', 'application/json')
      .then(response => {
        const registerMessage = JSON.parse(response.text)

        expect(registerMessage.message).toEqual(issue)
        expect(registerMessage.timeStamp).toBeTruthy()
      })
  })
})
