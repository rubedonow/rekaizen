const api = require('../api')
const request = require('supertest')

describe('Api', () => {
  test('retrieves translation', async () => {
    await request(api)
      .post('/translate')
      .set('Accept', 'application/json')
      .then(response => {
        const translations = JSON.parse(response.text)

        expect(translations.home.rekaizen).toEqual('ReKaizen')
      })
  })
})
