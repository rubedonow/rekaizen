const TranslationsService = require('../translations/Service')

class TranslateAll {
  static do () {
    return TranslationsService.retrieveAll()
  }
}

module.exports = TranslateAll
