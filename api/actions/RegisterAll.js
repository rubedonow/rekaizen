const RegisterService = require('../registrations/Service')

class RegisterAll {
  static createRegistry (message) {
    return RegisterService.createRegistry(message)
  }
}

module.exports = RegisterAll
