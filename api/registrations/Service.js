const RegisterCollection = require('./Collection')

class RegisterService {
  static createRegistry (message) {
    const timeStamp = new Date().getTime()

    return RegisterCollection.createRegistry(timeStamp, message)
  }
}

module.exports = RegisterService
