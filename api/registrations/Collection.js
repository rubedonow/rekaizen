const messages = []

class RegisterCollection {
  static createRegistry (timeStamp, message) {
    const registry = { message, timeStamp }
    messages.push(registry)

    return registry
  }
}

module.exports = RegisterCollection
