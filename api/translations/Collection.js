class TranslationsCollection {
  static retrieveAll () {
    return {
      home: {
        rekaizen: 'ReKaizen',
        startButton: 'Empieza!'
      },
      issue: {
        description: 'Un Tema es el objeto de análisis de una retrospectiva. Asegúrate que está propuesto de manera neutra y concisa para garantizar un análisis sin sesgos iniciales.',
        placeholder: 'Escriba el tema...',
        propose: 'Proponer',
        title: 'Éste es el tema',
        statement: 'Cómo squad confirmamos que queremos empezar el análisis de éste tema',
        confirm: 'Confirmar',
        cancel: 'Cancelar'
      },
      improvementAction: {
        title: 'Acciones de mejora',
        confirm: 'Añadir',
        placeholder: 'Introduce una acción de mejora SMART',
        buttonText: '+',
        smartGuide: {
          title: 'SMART',
          definition: 'Uno de los efectos de una retrospectiva son las acciones de mejora propuestas. Para que sean efectivas y relevantes en la mejora continua de una squad es importante que estén pensadas y expresadas de manera SMART.',
          specificTitle: 'Especifico',
          specific: 'Las acciones de mejora son expresadas de manera específica. Es importante asegurarse de que el planteamiento es claro y entendible por toda la squad.',
          measurableTitle: 'Medible',
          measurable: 'La squad debe ser capaz de medir el progreso de la acción de mejora propuesta y saber en qué punto ha logrado completarla.',
          attainableTitle: 'Realizable',
          attainable: 'La acción de mejora debe ser alcanzable por la squad. Tanto en términos de plazos, capacidades, habilidades, presupuesto... ',
          relevantTitle: 'Relevante',
          relevant: 'La squad debe reconocer la acción como algo relevante para su mejora continua. De lo contrario estaremos dirigiendo nuestros esfuerzos hacia acciones sin un impacto significativo.',
          timeboxedTitle: 'Tiempo delimitado',
          timeboxed: 'Establecer un tiempo para probar la acción de mejora ayudará a la squad a tener claro cuando quiere haber completado la acción.',
          video: 'https://www.youtube.com/watch?v=beqHx02U1iA'
        }
      },
      impression: {
        title: 'Impresiones',
        placeholder: 'Escriba una impresión...',
        buttonText: '+'
      },
    }
  }
}

module.exports = TranslationsCollection
