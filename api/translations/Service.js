const TranslationsCollection = require('./Collection')
class TranslationsService {
  static retrieveAll () {
    return TranslationsCollection.retrieveAll()
  }
}

module.exports = TranslationsService
